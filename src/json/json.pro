TARGET = json
TEMPLATE = lib

QT -= core gui

TWIPS_ROOT = $$PWD/../..

include($$TWIPS_ROOT/pri_files/lib.pri)

HEADERS = json.hpp \
    json.h
