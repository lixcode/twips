import QtQuick 2.0

import Twips.Network 1.0

Rectangle {
	color: "#ffffff"

	property bool canGoForward: true
	property bool blocked: false

	Rectangle {
		anchors {
			top: parent.top
			left: parent.left
			right: parent.right
		}
		height: 21
		color: "#f3f3f3"
	}
}
