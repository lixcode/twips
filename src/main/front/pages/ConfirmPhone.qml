import QtQuick 2.0
import QtQuick.Layouts 1.3

import Twips.Network 1.0

import "../ui" as UI

PageBase {
	id: page
	property Response response: null
	property RequestJwtResPL payload: null
	property int queryId: 0

	property string token: null

	function recieve(_res) {
		response = _res
		payload = _res.payload

		if (queryId != request.queryId) {
			return
		}

		if (response.code == 200) {
			socket.ready.disconnect(recieve)
			infoPage.jwt = payload.jwt
			infoPage.focusMe()
		} else {
			messageDialog.title = qsTr("Error occurer")
			messageDialog.text = response.description
			messageDialog.visible = true
		}

		blocked = false
		response.parent = null
	}

	function focusMe() {
		socket.ready.connect(recieve)
		stackView.currentIndex = 2
	}

	function toNext() {
		var code = codeInput.getCode()

		if (/^\d{4}$/.test(code)) {
			requestPL.code = code
		}

		queryId = socket.send(request)
	}

	Request {
		id: request
		command: "request_jwt"
		payload: RequestJwtReqPL {
			id: requestPL
			type: operationType
			resetPassword: false
			restore: false
			token: page.token
		}
	}

	RowLayout {
		anchors.fill: parent

		Item {
			Layout.fillWidth: true
		}

		ColumnLayout {
			Layout.preferredWidth: 540
			Layout.maximumWidth: 540
			Layout.fillHeight: true

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 108
			}

			UI.Header {
				text: qsTr("Confirm your number")
				Layout.alignment: Qt.AlignHCenter
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 20
			}

			UI.Header {
				text: qsTr("Twips send you private message on your other device. Enter the 4 digits from message")
				color: "#BDBDBD"
				Layout.alignment: Qt.AlignHCenter
				Layout.maximumWidth: 400
				wrapMode: Text.WrapAtWordBoundaryOrAnywhere
				font.bold: false
				horizontalAlignment: Text.Center
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 57
			}

			UI.CodeInput {
				id: codeInput

				Layout.alignment: Qt.AlignHCenter

				onCodeComplete: toNext()
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 150
			}

			RowLayout {
				UI.Button {
					text: qsTr("Prev")

					Layout.alignment: Qt.AlignRight

					onClicked: {
						socket.ready.disconnect(recieve)
						phonePage.focusMe()
					}
				}

				Item {
					Layout.fillWidth: true
				}

				UI.Button {
					text: qsTr("Next")

					Layout.alignment: Qt.AlignRight

					onClicked: toNext()
				}
			}

			Item {
				Layout.fillHeight: true
			}
		}

		Item {
			Layout.fillWidth: true
		}
	}
}
