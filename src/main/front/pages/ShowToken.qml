import QtQuick 2.0
import QtQuick.Layouts 1.3

import "../ui" as UI

PageBase {
	property alias tokenMessage: tokenMessage.text
	property alias tokenItself: tokenItself.text

	RowLayout {
		anchors.fill: parent

		Item {
			Layout.fillWidth: true
		}

		ColumnLayout {
			Layout.preferredWidth: 540
			Layout.maximumWidth: 540
			Layout.fillHeight: true

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 108
			}

			UI.Header {
				id: tokenMessage
				text: qsTr("Token message here")
				Layout.alignment: Qt.AlignHCenter
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 20
			}

			UI.Header {
				id: tokenItself
				text: qsTr("Token itself here z zx z xcz xc zxc zxczxczxcz xc zx cz xczxc zcx z xczxczxczxc zxczxc zxc zx c")
				color: "#BDBDBD"
				Layout.alignment: Qt.AlignHCenter
				Layout.maximumWidth: 400
				wrapMode: Text.WrapAtWordBoundaryOrAnywhere
				font.bold: false
			}

			Item {
				Layout.fillHeight: true
			}
		}

		Item {
			Layout.fillWidth: true
		}
	}
}
