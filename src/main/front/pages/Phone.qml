import QtQuick 2.0
import QtQuick.Layouts 1.3

import Twips.Network 1.0

import "../ui" as UI

PageBase {
	property Response response: null
	property RequestCodeResPL payload: null
	property int queryId: 0

	function recieve(_res) {
		response = _res
		payload = _res.payload

		if (queryId != request.queryId) {
			return
		}

		if (response.code == 200) {
			confirmPage.token = payload.token
			socket.ready.disconnect(recieve)
			confirmPage.focusMe()
		} else {
			messageDialog.title = qsTr("Error occurer")
			messageDialog.text = response.description
			messageDialog.visible = true
		}

		blocked = false
		response.parent = null
	}

	function focusMe() {
		socket.ready.connect(recieve)
		stackView.currentIndex = 1
	}

	RowLayout {
		anchors.fill: parent

		Item {
			Layout.fillWidth: true
		}

		ColumnLayout {
			Layout.preferredWidth: 540
			Layout.maximumWidth: 540
			Layout.fillHeight: true

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 108
			}

			UI.Header {
				text: qsTr("Select your country")
				Layout.alignment: Qt.AlignVCenter
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 57
			}

			UI.ComboBox {
				id: comboBox
				Layout.fillWidth: true

				model: ListModel {
					id: countries

					ListElement {
						text: "Russian Federation"
						code: 7
					}
					ListElement {
						text: "Ukraine"
						code: 380
					}
					ListElement {
						text: "Belarus"
						code: 375
					}
					ListElement {
						text: "Kazakhstan"
						code: 7
					}
					ListElement {
						text: "Poland"
						code: 48
					}
					ListElement {
						text: "Moldova"
						code: 373
					}
					ListElement {
						text: "Uzbekistan"
						code: 998
					}
					ListElement {
						text: "Test 1"
						code: 1
					}
					ListElement {
						text: "Test 2"
						code: 2
					}
				}
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 30
			}

			UI.NumberInput {
				id: numberInput
				code: "+" + countries.get(comboBox.currentIndex).code

				Layout.fillWidth: true
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 50
			}

			UI.Button {
				text: qsTr("Next")

				Layout.alignment: Qt.AlignRight

				onClicked: {
					if (blocked)
						return

					if (/^\(\d{3}\)\s\d{3}\s\d\d\s\d\d/.test(numberInput.text)) {
						var tmp = numberInput.code + numberInput.text
						var phone = tmp.replace(/[^\d]/g, "")

						requestPL.phone = phone

						queryId = socket.send(request)
						blocked = true
					}
				}

				Request {
					id: request
					command: "request_code"
					payload: RequestCodeReqPL {
						id: requestPL
						type: operationType
					}
				}
			}

			Item {
				Layout.fillHeight: true
			}
		}

		Item {
			Layout.fillWidth: true
		}
	}
}
