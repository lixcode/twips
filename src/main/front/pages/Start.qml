import QtQuick 2.0
import QtQuick.Layouts 1.3

import Twips.Network 1.0

import "../ui" as UI

PageBase {
	property Response response: null
	property HelloResPL payload: null
	property int queryId: 0

	function recieve(_res) {
		response = _res
		payload = _res.payload

		if (queryId != request.queryId) {
			return
		}

		if (response.code == 200) {
			if (payload.message == "world") {
				socket.ready.disconnect(recieve)
				phonePage.focusMe()
			}
		} else {
			messageDialog.title = qsTr("Error occurer")
			messageDialog.text = response.description
			messageDialog.visible = true
		}

		blocked = false
		response.parent = null
	}

	function focusMe() {
		socket.ready.connect(recieve)
		stackView.currentIndex = 0
	}

	ColumnLayout {
		anchors.fill: parent

		Item {
			Layout.fillHeight: true
		}

		Rectangle {
			width: 360
			height: 360
			radius: 180
			color: "#1169BE"

			Layout.alignment: Layout.Center

			Text {
				text: "TWIPS"
				font.family: "Arial"
				font.pointSize: 60
				anchors.centerIn: parent
				color: "white"
			}
		}

		Item {
			Layout.fillHeight: true
			Layout.maximumHeight: 45
		}

		RowLayout {
			Layout.fillWidth: true

			Item {
				Layout.fillWidth: true
			}

			Text {
				width: 360
				height: 40
				text: "Community unites people with common interests or hobbies."
					  + "In community you can ceate new group chats and news channels."
				font.family: "Noto Sans"
				horizontalAlignment: Text.AlignHCenter
				font.pointSize: 10.5
				wrapMode: Text.WrapAtWordBoundaryOrAnywhere
				color: "#292929"

				Layout.alignment: Layout.Center
				Layout.maximumWidth: 360
			}

			Item {
				Layout.fillWidth: true
			}
		}

		Item {
			Layout.fillHeight: true
			Layout.maximumHeight: 90
		}

		UI.Button {
			text: "Start messaging"
			width: 180

			Layout.alignment: Layout.Center
			Layout.preferredWidth: 186

			onClicked: {
				if (blocked) {
					return
				}

				queryId = socket.send(request)
				blocked = true
			}

			Request {
				id: request

				command: "hello"
			}
		}

		Item {
			Layout.fillHeight: true
		}
	}
}
