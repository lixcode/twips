import QtQuick 2.0
import QtQuick.Layouts 1.3

import Twips.Network 1.0

import "../ui" as UI

PageBase {
	id: changeInfo
	property Response response: null
	property int queryId: 0

	property string jwt: null

	function recievePresence(_res) {
		response = _res

		if (queryId != request.queryId) {
			return
		}

		if (response.code == 200) {
			queryId = socket.send(profileRequest)
		} else {
			messageDialog.title = qsTr("Error occurer")
			messageDialog.text = response.description
			messageDialog.visible = true
			blocked = false
		}

		response.parent = null
	}

	Request {
		id: profileRequest

		command: "profile_update"
		jwt: changeInfo.jwt

		payload: ProfileUpdateReqPL {
			id: profilePL
		}
	}

	function recieveProfile(_res) {
		response = _res

		if (queryId != profileRequest.queryId) {
			return
		}

		if (response.code == 200) {
			socket.ready.disconnect(recieveProfile)
			socket.ready.disconnect(recievePresence)
			tokenPage.tokenMessage = qsTr("Token given")
			tokenPage.tokenItself = jwt
			database.saveToken(jwt)
			stackView.currentIndex = 4
		} else {
			messageDialog.title = qsTr("Error occurer")
			messageDialog.text = response.description
			messageDialog.visible = true
		}

		blocked = false
		response.parent = null
	}

	function focusMe() {
		socket.ready.connect(recievePresence)
		socket.ready.connect(recieveProfile)
		stackView.currentIndex = 3
	}

	RowLayout {
		anchors.fill: parent

		Item {
			Layout.fillWidth: true
		}

		ColumnLayout {
			Layout.preferredWidth: 540
			Layout.maximumWidth: 540
			Layout.fillHeight: true
			spacing: 30

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 108
			}

			UI.Header {
				text: qsTr("Enter your info")
				Layout.alignment: Qt.AlignHCenter
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 20
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 100
			}

			UI.InputWithLimit {
				id: name
				Layout.fillWidth: true
				placeHolder: qsTr("First name")
			}

			UI.InputWithLimit {
				id: firstName
				Layout.fillWidth: true
				placeHolder: qsTr("Surname name (optional)")
			}

			UI.NickNameInput {
				id: nickName
				Layout.fillWidth: true
				placeHolder: qsTr("nickname (optional)")
			}

			Item {
				Layout.fillHeight: true
				Layout.maximumHeight: 20
			}

			UI.Button {
				text: qsTr("Next")

				Layout.alignment: Qt.AlignRight

				onClicked: {
					var valid = true;

					if (name.text.lenght < 5) valid = false;
					if (nickName.text.length > 0) {
						if (nickName.text.length < 5) valid = false
						if (!/^[a-z]+$/.test(nickName.text)) valid = false
					}

					if (blocked || !valid)
						return

					profilePL.surname = name.text
					profilePL.firstName = firstName.text
					profilePL.name = nickName.text
					queryId = socket.send(request)
				}

				Request {
					id: request
					command: "presence"
					payload: PresenceReqPL {
						jwt: changeInfo.jwt
					}
				}
			}

			Item {
				Layout.fillHeight: true
			}
		}

		Item {
			Layout.fillWidth: true
		}
	}
}
