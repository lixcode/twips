import QtQuick 2.0

Text {
	font.family: "Noto Sans"
	font.pointSize: 12
	font.bold: true
	color: "#292929"

	text: qsTr("Header")
}
