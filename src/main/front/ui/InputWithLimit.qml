import QtQuick 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
	id: input

	height: 25

	property alias placeHolder: textField.placeholderText
	property real leftMargin: -9
	property alias text: textField.text

	TextField {
		id: textField
		anchors.fill: parent
		anchors.leftMargin: leftMargin

		font.family: "NotoSans"
		font.pointSize: 12

		textColor: "#292929"

		style: TextFieldStyle {
			background: null
			placeholderTextColor: "#BDBDBD"
		}

		maximumLength: 35
	}

	Rectangle {
		id: bottomLine

		anchors {
			left: parent.left
			right: remains.left
			rightMargin: 2
			bottom: parent.bottom
		}

		height: 1

		color: "#E0E0E0"
	}

	Text {
		id: remains
		color: "#E0E0E0"
		text: 35 - textField.text.length
		anchors.verticalCenter: parent.bottom
		anchors.right: parent.right
	}
}
