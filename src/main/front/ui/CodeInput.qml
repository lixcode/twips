import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
	width: 135
	height: 25

	function getCode() {
		var code = ""

		for (var i = 0; i < 4; i++) {
			code += repeater.itemAt(i).tField.text
		}

		return code
	}

	signal codeComplete()

	Header {
		id: enterDigits
		text: qsTr("Enter digits")
		color: "#BDBDBD"
		font.bold: false
		anchors.centerIn: parent
	}

	Row {
		spacing: 5

		Repeater {
			id: repeater
			model: 4

			Item {
				width: 35
				height: 25

				property alias tField: textField

				Rectangle {
					anchors {
						left: parent.left
						right: parent.right
						bottom: parent.bottom
					}

					height: 1
					color: "#E0E0E0"
				}

				TextField {
					id: textField
					font.family: "NotoSans"
					font.pointSize: 14
					font.bold: true
					maximumLength: 1
					horizontalAlignment: TextInput.AlignHCenter
					anchors.fill: parent

					textColor: "#292929"

					style: TextFieldStyle {
						background: null
					}

					onActiveFocusChanged: {
						if (activeFocus) {
							enterDigits.visible = false
						}
					}

					Keys.onPressed: {
						if (/^\d$/.test(event.text)) {
							var next = index != 3 ? repeater.itemAt(
														index + 1).tField : null

							if (text.length == 0 || selectionStart == 0) {
								text = event.text
								if (!!next)
									next.select(0, 0)
							} else if (!!next) {
								next.text = event.text
								next.select(1, 1)
							}

							if (!!next)
								next.forceActiveFocus()
							else
								codeComplete()
						}
						event.accepted = true
					}
				}
			}
		}
	}
}
