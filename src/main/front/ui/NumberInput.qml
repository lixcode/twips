import QtQuick 2.11
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Item {
	id: input

	property alias code: code.text
	property alias text: textField.text
	height: 25

	Text {
		id: code

		font.family: "NotoSans"
		verticalAlignment: Text.AlignVCenter
		font.pointSize: 11
		font.bold: true

		anchors {
			top: parent.top
			bottom: bottomLine.top
			left: parent.left
			leftMargin: 6
			topMargin: -4
		}
	}

	TextField {
		id: textField

		anchors {
			top: parent.top
			left: code.right
			right: parent.right
			bottom: bottomLine.top
		}
		font.family: "NotoSans"
		font.pointSize: 11

		placeholderText: qsTr("Enter your phone number here")

		textColor: "#292929"

		style: TextFieldStyle {
			background: null
			placeholderTextColor: "#BDBDBD"
		}

		MouseArea {
			anchors.fill: parent
			// will be good to change mask on country change, but и так сойдёт
			onClicked: {
				visible = false
				parent.inputMask = "(999) 999 99 99"
				parent.select(1,1)
				parent.forceActiveFocus()
			}
		}
	}

	Rectangle {
		id: bottomLine

		anchors {
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}

		height: 1

		color: "#E0E0E0"
	}
}
