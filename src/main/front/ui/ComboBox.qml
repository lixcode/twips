import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

ComboBox {
	id: control

	onCurrentIndexChanged: console.log(currentIndex)

	displayText: model.get(currentIndex).text
	font.bold: true

	delegate: ItemDelegate {
		width: control.width
		contentItem: RowLayout {

			Text {
				text: model.text
				color: "#292929"
				elide: Text.ElideRight
				verticalAlignment: Text.AlignVCenter

				Component.onCompleted: {
					font = control.font
					font.bold = true
				}
			}

			Item {
				Layout.fillWidth: true
			}

			Text {
				text: "+" + model.code
				color: "#292929"
				verticalAlignment: Text.AlignVCenter

				Component.onCompleted: {
					font = control.font
					font.bold = false
				}
			}
		}

		highlighted: control.highlightedIndex === index

		background: Rectangle {
			color: highlighted ? "#E1EEF9" : "#FFFFFF"
		}
	}

	indicator: Canvas {
		id: canvas
		x: control.width - width - control.rightPadding
		y: control.topPadding + (control.availableHeight - height) / 2
		width: 12
		height: 12
		contextType: "2d"

		Connections {
			target: control
			onPressedChanged: canvas.requestPaint()
		}

		onPaint: {
			context.reset()

			// Center arrow
			if (popup.visible) {
				context.translate(0, height / 4)
			} else {
				context.translate(width / 4, 0)
			}

			// Draw arrow
			context.moveTo(0, 0)
			context.lineTo(width / 2, height / 2)

			if (popup.visible) {
				context.lineTo(width, 0)
			} else {
				context.lineTo(0, height)
			}

			context.strokeStyle = "#292929"
			context.lineWidth = 2
			context.stroke()
		}
	}

	contentItem: Text {
		leftPadding: 13
		rightPadding: control.indicator.width + control.spacing

		text: control.displayText
		font: control.font
		color: control.pressed ? "#000000" : "#292929"
		verticalAlignment: Text.AlignVCenter
		elide: Text.ElideRight
	}

	background: Rectangle {
		id: bgRect
		implicitWidth: 120
		implicitHeight: 40
		border.color: "#bdbdbd"
		border.width: 1
		radius: 5

		Rectangle {
			id: bgReplacer
			visible: popup.visible

			anchors {
				left: parent.left
				right: parent.right
				bottom: parent.bottom
			}
			height: parent.radius
			color: parent.color

			Rectangle {
				width: bgRect.border.width
				anchors {
					top: parent.top
					bottom: parent.bottom
				}
				color: bgRect.border.color
			}

			Rectangle {
				width: bgRect.border.width
				anchors {
					top: parent.top
					bottom: parent.bottom
					right: parent.right
				}
				color: bgRect.border.color
			}
		}
	}

	popup: Popup {
		id: popup

		y: control.height - 1
		width: control.width
		implicitHeight: contentItem.implicitHeight
		padding: 1
		height: control.height * 7

		contentItem: Item {

			Canvas {
				id: opMask
				anchors.fill: parent
				visible: false

				property real radius: 4

				onPaint: {
					var ctx = getContext("2d")

					ctx.clearRect(0, 0, width, height)
					ctx.moveTo(0, 0)
					ctx.lineTo(width, 0)
					ctx.lineTo(width, height - radius)
					ctx.arcTo(width, height, width - radius, height, radius)
					ctx.lineTo(radius, height)
					ctx.arcTo(0, height, 0, height - radius, radius)
					ctx.closePath()

					ctx.fillStyle = "red"
					ctx.fill()
				}
			}

			RowLayout {
				id: optList
				opacity: 0
				anchors.fill: parent
				anchors.topMargin: 4

				implicitHeight: listview.contentHeight
				spacing: 0

				ListView {
					id: listview
					clip: true
					model: control.popup.visible ? control.delegateModel : null
					currentIndex: control.highlightedIndex

					Layout.fillWidth: true
					Layout.fillHeight: true
				}

				Rectangle {
					color: "#E1EEF9"

					Layout.fillHeight: true
					Layout.minimumWidth: 7
					Layout.maximumWidth: 7

					ScrollIndicator {
						id: verticalIndicator
						active: true
						orientation: Qt.Vertical
						size: listview.height / listview.contentHeight
						position: listview.contentY / listview.contentHeight

						anchors.fill: parent

						background: null
						contentItem: Rectangle {
							width: 5
							implicitHeight: 10
							radius: 3
							x: 1
							color: "#2F4858"
						}
					}
				}
			}

			OpacityMask {
				anchors.fill: parent
				source: optList
				maskSource: opMask
			}
		}

		background: Rectangle {
			id: popBg
			border.color: "#bdbdbd"
			radius: 5

			Rectangle {
				anchors {
					top: parent.top
					left: parent.left
					right: parent.right
				}
				height: parent.radius
				color: parent.color

				Rectangle {
					height: popBg.border.width
					anchors {
						left: parent.left
						right: parent.right
					}
					color: popBg.border.color
				}

				Rectangle {
					width: popBg.border.width
					anchors {
						top: parent.top
						bottom: parent.bottom
					}
					color: popBg.border.color
				}

				Rectangle {
					width: popBg.border.width
					anchors {
						top: parent.top
						bottom: parent.bottom
						right: parent.right
					}
					color: popBg.border.color
				}
			}
		}
	}
}
