import QtQuick 2.0
import QtQuick.Controls 2.4

Button {
	id: control
	text: qsTr("Button")

	font.family: "Noto Sans"
	font.bold: true
	font.pixelSize: 16

	contentItem: Text {
		id: textItem
		text: control.text
		font: control.font
		color: enabled ? "#3e88e9" : "#bdbdbd"
		horizontalAlignment: Text.AlignHCenter
		verticalAlignment: Text.AlignVCenter
		elide: Text.ElideRight
	}

	background: Rectangle {
		implicitHeight: 42
		implicitWidth: 100
		border {
			width: 1
			color: enabled ? "#3e88e9" : "#bdbdbd"
		}
		color: "#ffffff"
		radius: height * 0.5
	}
}
