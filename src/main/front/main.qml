import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.1

import Twips.Main 1.0
import Twips.Network 1.0

import "pages" as Pages


Window {
	visible: true
	width: 880
	height: 600
	title: qsTr("Hello World")

	property string operationType: "register"

	DataBase {
		id: database
	}

	TcpSocket {
		id: socket
	}

	StackLayout {
		id: stackView
		anchors.fill: parent
		visible: false

		currentIndex: 4

		Pages.Start { id: startPage }
		Pages.Phone { id: phonePage }
		Pages.ConfirmPhone { id: confirmPage }
		Pages.ChangeInfo { id: infoPage }
		Pages.ShowToken { id: tokenPage }

		Component.onCompleted: {
			var token = database.getToken();

			if (!!token) {
				tokenPage.tokenMessage = qsTr("Token loaded from database")
				tokenPage.tokenItself = token
				currentIndex = 4
			}
			else {
				startPage.focusMe()
			}
			// Show the needed page
			stackView.visible = true
		}
	}

	MessageDialog {
		id: messageDialog
	}
}
