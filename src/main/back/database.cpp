#include "database.h"

#include <QDebug>
#include <QFile>
#include <QSqlError>
#include <QSqlQuery>

DataBase::DataBase(QObject * parent)
	: QObject(parent) {
	db = QSqlDatabase::addDatabase("QSQLITE");
	db.setDatabaseName("twips.db");

	if (!db.open()) {
		qDebug() << db.lastError();

		invalid = true;
		return;
	}

	QStringList tables = db.tables();

	if (!tables.contains("tokens", Qt::CaseInsensitive)) {
		auto query = queryFromFile(":/create-db-v1.sql");

		if (!query.exec()) {
			qDebug() << db.lastError();

			invalid = true;
			return;
		}
	}

	invalid = false;
}

bool DataBase::ikOk() {
	return !invalid;
}

QString DataBase::getToken() {
	if (invalid) {
		return {};
	}

	auto query = queryFromFile(":/get-last-token.sql");

	if (query.exec() && query.first()) {
		return query.value("token").toString();
	}

	return {};
}

QString DataBase::saveToken(const QString & token) {
	if (invalid) {
		return "Invalid database";
	}

	auto query = queryFromFile(":/save-token.sql");

	query.addBindValue(token);
	query.exec();

	return query.lastError().text();
}

QSqlQuery DataBase::queryFromFile(const QString & path) {
	QSqlQuery   query{db};
	QFile       file{path};
	QTextStream stream{&file};

	if (file.open(QFile::ReadOnly)) {
		query.prepare(stream.readAll());
	}

	return query;
}
