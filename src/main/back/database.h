#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QSqlDatabase>

/**
 * @brief The DataBase class will release access to SQLite database
 */
class DataBase : public QObject
{
	Q_OBJECT
public:
	/**
	 * @brief DataBase opens the database from HDD
	 * @param parent is the parent of QObject
	 */
	explicit DataBase(QObject * parent = nullptr);

	/**
	 * @brief ikOk return true if database is opened successful
	 * @return true if database is opened successful, otherwise false
	 */
	Q_INVOKABLE bool ikOk();

	/**
	 * @brief getToken returns the last token from database if so exists
	 * @return the last token from database if so exists
	 */
	Q_INVOKABLE QString getToken();

	/**
	 * @brief saveToken saves the token to database
	 * @param token is the token to save
	 * @return the sql error on error, empty string on success
	 */
	Q_INVOKABLE QString saveToken(const QString & token);

signals:

public slots:

private:
	/**
	 * @brief queryFromFile create a query from file content
	 * @param path is the path to the sql file
	 * @return created query
	 */
	QSqlQuery queryFromFile(const QString & path);

private:
	/// @brief db is the database to work with
	QSqlDatabase db;

	/// @brief invalid block the request if database was not opened
	bool invalid = true;
};

#endif  // DATABASE_H
