QT += core widgets gui qml sql

TARGET = twips
TEMPLATE = app

TWIPS_ROOT = $$PWD/../..

include($$TWIPS_ROOT/pri_files/app.pri)

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH += $$DESTDIR

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

SOURCES += back/main.cpp \
    back/database.cpp

RESOURCES += front/qml.qrc \
    sql/sql.qrc

HEADERS += \
    back/database.h
