#ifndef NETWORKPLUGIN_H
#define NETWORKPLUGIN_H

#include <QQmlExtensionPlugin>



class NetworkPlugin : public QQmlExtensionPlugin
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID QQmlExtensionInterface_iid)

public:
	explicit NetworkPlugin(QObject * parent = nullptr);

	// QQmlTypesExtensionInterface interface
public:
	void registerTypes(const char * uri) override;
};

#endif  // NETWORKPLUGIN_H
