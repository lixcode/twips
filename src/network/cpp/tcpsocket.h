#ifndef TCPSOCKET_H
#define TCPSOCKET_H

#include <QList>
#include <QMutex>
#include <QObject>
#include <QTcpSocket>

class Request;
class Response;

struct RequestItem
{
	Request * request = nullptr;  //< The request to send
	bool      inited  = false;    //< The requst was sended to server

	RequestItem(Request * req) {
		request = req;
	}

	bool operator==(const RequestItem & other) {
		return request == other.request;
	}
};

/**
 * @brief The WebSocket class is a interface between QML and QWebSocket
 */
class TcpSocket : public QObject
{
	Q_OBJECT
public:
	explicit TcpSocket(QObject * parent = nullptr);

signals:
	/**
	 * @brief ready get sended if a request from queue get ready
	 * @param response is the response in twips format
	 */
	void ready(Response * response);

public slots:
	/**
	 * @brief send send a request to server
	 * @param request is the request json
	 * @return the request_id
	 */
	int send(Request * request);

	/**
	 * @brief cancel cancels the request on timeout
	 * @param request_id the id of request to cancel
	 */
	void cancel(int request_id);

	/**
	 * @brief triggered if a message was fully received
	 * @param message is the recieved message
	 */
	void binaryMessageReceived(const QByteArray & message);

private slots:
	/**
	 * @brief connected handles connected of QTcpSocket
	 */
	void connected();

	/**
	 * @brief disconnected handles disconnected of QTcpSocket
	 */
	void disconnected();

	/**
	 * @brief error handles error of QTcpSocket
	 */
	void error(QAbstractSocket::SocketError);

	/**
	 * @brief readRead handles readRead from QTcpSocket
	 */
	void readyRead();

	/**
	 * @brief bytesReady is called when a request is ready
	 * @param bytes is the number of bytes, ready to read now
	 */
	void bytesReady(qint64 bytes);

	/**
	 * @brief processNow threadsafe process of queue
	 */
	void processNow();

	/**
	 * @brief removeRequest threadsafe removing of requests in queue
	 */
	void removeRequest(const QString & id);

private:
	/**
	 * @brief socket is the socket to connect to server
	 */
	QTcpSocket socket;

	/**
	 * @brief queue of needed requests
	 */
	QList<RequestItem> list;

	/**
	 * @brief lastId is the id of last processed request
	 */
	int lastId = 0;

	/**
	 * @brief queueMutex make QQueue thread safe
	 */
	QMutex listMutex;

	/**
	 * @brief messageLength is the length of the current message
	 */
	long messageLength = 0;

	/**
	 * @brief messageBuffer is the buffer which collects the recived data
	 */
	QByteArray messageBuffer;
};

#endif  // TCPSOCKET_H
