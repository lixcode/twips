#include "requestcodereqpl.h"

RequestCodeReqPL::RequestCodeReqPL(QObject * parent)
	: RequestPayLoad(parent) {}

QString RequestCodeReqPL::phone() const {
	return m_phone;
}

QString RequestCodeReqPL::type() const {
	return m_type;
}

Json RequestCodeReqPL::toJson() {
	return {{"phone", m_phone.toStdString()}, {"type", m_type.toStdString()}};
}

void RequestCodeReqPL::setPhone(QString phone) {
	if (m_phone == phone)
		return;

	m_phone = phone;
	emit phoneChanged(m_phone);
}

void RequestCodeReqPL::setType(QString type) {
	if (m_type == type)
		return;

	m_type = type;
	emit typeChanged(m_type);
}
