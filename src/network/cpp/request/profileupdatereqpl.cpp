#include "profileupdatereqpl.h"

#include <json/json.hpp>

using json = nlohmann::json;

ProfileUpdateReqPL::ProfileUpdateReqPL(QObject * parent)
	: RequestPayLoad(parent) {}

QString ProfileUpdateReqPL::name() const {
	return m_name;
}

QString ProfileUpdateReqPL::firstName() const {
	return m_firstName;
}

QString ProfileUpdateReqPL::surname() const {
	return m_surname;
}

Json ProfileUpdateReqPL::toJson() {
	return {{"name", m_name.toStdString()},
			{"first_name", m_firstName.toStdString()},
			{"surname", m_surname.toStdString()}};
}

void ProfileUpdateReqPL::setName(QString name) {
	if (m_name == name)
		return;

	m_name = name;
	emit nameChanged(m_name);
}

void ProfileUpdateReqPL::setFirstName(QString firstName) {
	if (m_firstName == firstName)
		return;

	m_firstName = firstName;
	emit firstNameChanged(m_firstName);
}

void ProfileUpdateReqPL::setSurname(QString surname) {
	if (m_surname == surname)
		return;

	m_surname = surname;
	emit surnameChanged(m_surname);
}
