#ifndef REQUEST_H
#define REQUEST_H

#include <json/json.h>

#include <QObject>


class RequestPayLoad;

/**
 * @brief The Request class contains all request data
 */
class Request : public QObject
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString command         READ command WRITE setCommand NOTIFY commandChanged)
	Q_PROPERTY(QString queryId         READ queryId WRITE setQueryId NOTIFY queryIdChanged)
	Q_PROPERTY(QString jwt             READ jwt     WRITE setJwt     NOTIFY jwtChanged)
	Q_PROPERTY(RequestPayLoad* payload READ payload WRITE setPayload NOTIFY payloadChanged)
	// clang-format on

public:
	explicit Request(QObject * parent = nullptr);

	QString command() const;
	QString queryId() const;
	QString jwt() const;

	RequestPayLoad * payload() const;

	/**
	 * @brief toJson make new json object with class fields
	 * @return new json object
	 */
	Json toJson();

signals:
	void commandChanged(QString command);
	void queryIdChanged(QString queryId);
	void jwtChanged(QString jwt);
	void payloadChanged(RequestPayLoad * payload);

public slots:
	void setCommand(QString command);
	void setQueryId(QString queryId);
	void setJwt(QString jwt);
	void setPayload(RequestPayLoad * payload);

private:
	QString m_command;
	QString m_queryId;
	QString m_jwt;

	RequestPayLoad * m_payload = nullptr;
};

#endif  // REQUEST_H
