#ifndef REQUESTCODEREQPL_H
#define REQUESTCODEREQPL_H

#include "requestpayload.h"

/**
 * @brief The RequestCodeReqPL class is playload of request_code request
 */
class RequestCodeReqPL : public RequestPayLoad
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString phone READ phone WRITE setPhone NOTIFY phoneChanged)
	Q_PROPERTY(QString type  READ type  WRITE setType  NOTIFY typeChanged)
	// clang-format on

public:
	explicit RequestCodeReqPL(QObject * parent = nullptr);

	QString phone() const;
	QString type() const;

	/**
	 * @brief toJson make new json object with class fields
	 * @return new json object
	 */
	Json toJson();

signals:
	void phoneChanged(QString phone);
	void typeChanged(QString type);

public slots:
	void setPhone(QString phone);
	void setType(QString type);

private:
	QString m_phone;
	QString m_type;
};

#endif  // REQUESTCODEREQPL_H
