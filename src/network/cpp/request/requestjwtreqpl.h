#ifndef REQUESTJWTREQPL_H
#define REQUESTJWTREQPL_H

#include "requestpayload.h"

/**
 * @brief The RequestJwtReqPL class is payload of request_jwt request
 */
class RequestJwtReqPL : public RequestPayLoad
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString type       READ type          WRITE setType          NOTIFY typeChanged)
	Q_PROPERTY(QString code       READ code          WRITE setCode          NOTIFY codeChanged)
	Q_PROPERTY(bool resetPassword READ resetPassword WRITE setResetPassword NOTIFY resetPasswordChanged)
	Q_PROPERTY(bool restore       READ restore       WRITE setRestore       NOTIFY restoreChanged)
	Q_PROPERTY(QString token      READ token         WRITE setToken         NOTIFY tokenChanged)
	// clang-format on

public:
	explicit RequestJwtReqPL(QObject * parent = nullptr);

	QString type() const;
	QString code() const;
	bool    resetPassword() const;
	bool    restore() const;
	QString token() const;

	/**
	 * @brief toJson make new json object with class fields
	 * @return new json object
	 */
	Json toJson();

signals:
	void typeChanged(QString type);
	void codeChanged(QString code);
	void resetPasswordChanged(bool resetPassword);
	void restoreChanged(bool restore);
	void tokenChanged(QString token);

public slots:
	void setType(QString type);
	void setCode(QString code);
	void setResetPassword(bool resetPassword);
	void setRestore(bool restore);
	void setToken(QString token);

private:
	QString m_type;
	QString m_code;
	bool    m_resetPassword = false;
	bool    m_restore       = false;
	QString m_token;
};

#endif  // REQUESTJWTREQPL_H
