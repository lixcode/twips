#ifndef PROFILEUPDATEREQPL_H
#define PROFILEUPDATEREQPL_H

#include "requestpayload.h"

/**
 * @brief The ProfileUpdateReqPL class is payload of profile_update request
 */
class ProfileUpdateReqPL : public RequestPayLoad
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString name      READ name      WRITE setName      NOTIFY nameChanged)
	Q_PROPERTY(QString firstName READ firstName WRITE setFirstName NOTIFY firstNameChanged)
	Q_PROPERTY(QString surname   READ surname   WRITE setSurname   NOTIFY surnameChanged)
	// clang-format on

public:
	ProfileUpdateReqPL(QObject * parent = nullptr);

	QString name() const;
	QString firstName() const;
	QString surname() const;

	/**
	 * @brief toJson make new json object with class fields
	 * @return new json object
	 */
	Json toJson();

signals:
	void nameChanged(QString name);
	void firstNameChanged(QString firstName);
	void surnameChanged(QString surname);

public slots:
	void setName(QString name);
	void setFirstName(QString firstName);
	void setSurname(QString surname);

private:
	QString m_name;
	QString m_firstName;
	QString m_surname;
};

#endif  // PROFILEUPDATEREQPL_H
