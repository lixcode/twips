#include "requestjwtreqpl.h"

RequestJwtReqPL::RequestJwtReqPL(QObject * parent)
	: RequestPayLoad(parent) {}

QString RequestJwtReqPL::type() const {
	return m_type;
}

QString RequestJwtReqPL::code() const {
	return m_code;
}

bool RequestJwtReqPL::resetPassword() const {
	return m_resetPassword;
}

bool RequestJwtReqPL::restore() const {
	return m_restore;
}

QString RequestJwtReqPL::token() const {
	return m_token;
}

Json RequestJwtReqPL::toJson() {
	return {{"type", m_type.toStdString()},
			{"code", m_code.toStdString()},
			{"reset_password", m_resetPassword},
			{"restore", m_restore},
			{"token", m_token.toStdString()}};
}

void RequestJwtReqPL::setType(QString type) {
	if (m_type == type)
		return;

	m_type = type;
	emit typeChanged(m_type);
}

void RequestJwtReqPL::setCode(QString code) {
	if (m_code == code)
		return;

	m_code = code;
	emit codeChanged(m_code);
}

void RequestJwtReqPL::setResetPassword(bool resetPassword) {
	if (m_resetPassword == resetPassword)
		return;

	m_resetPassword = resetPassword;
	emit resetPasswordChanged(m_resetPassword);
}

void RequestJwtReqPL::setRestore(bool restore) {
	if (m_restore == restore)
		return;

	m_restore = restore;
	emit restoreChanged(m_restore);
}

void RequestJwtReqPL::setToken(QString token) {
	if (m_token == token)
		return;

	m_token = token;
	emit tokenChanged(m_token);
}
