#ifndef REQUESTPAYLOAD_H
#define REQUESTPAYLOAD_H

#include <json/json.h>

#include <QObject>

/**
 * @brief The RequestPayLoad class is a parent class for all request payloads
 */
class RequestPayLoad : public QObject
{
	Q_OBJECT
public:
	explicit RequestPayLoad(QObject * parent = nullptr);

	/**
	 * @brief toJson make new json object with class fields
	 * @return new json object
	 */
	virtual Json toJson() = 0;

signals:

public slots:
};

#endif  // REQUESTPAYLOAD_H
