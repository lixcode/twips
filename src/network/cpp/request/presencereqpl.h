#ifndef PRECENCEREQPL_H
#define PRECENCEREQPL_H

#include "requestpayload.h"

#include <json/json.hpp>

using json = nlohmann::json;

/**
 * @brief The PresenceReqPL class is payload of presence request
 */
class PresenceReqPL : public RequestPayLoad
{
	Q_OBJECT

	Q_PROPERTY(QString jwt READ jwt WRITE setJwt NOTIFY jwtChanged)

public:
	explicit PresenceReqPL(QObject * parent = nullptr);

	QString jwt() const;

	/**
	 * @brief toJson make new json object with class fields
	 * @return new json object
	 */
	Json toJson();

signals:
	void jwtChanged(QString jwt);

public slots:
	void setJwt(QString jwt);

private:
	QString m_jwt;
};

#endif  // PRECENCEREQPL_H
