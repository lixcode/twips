#include "presencereqpl.h"

PresenceReqPL::PresenceReqPL(QObject * parent)
	: RequestPayLoad(parent) {}

QString PresenceReqPL::jwt() const {
	return m_jwt;
}

Json PresenceReqPL::toJson() {
	return {{"jwt", m_jwt.toStdString()}};
}

void PresenceReqPL::setJwt(QString jwt) {
	if (m_jwt == jwt)
		return;

	m_jwt = jwt;
	emit jwtChanged(m_jwt);
}
