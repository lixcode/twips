#include "request.h"

#include "requestpayload.h"

#include <iostream>

Request::Request(QObject * parent)
	: QObject(parent) {}

QString Request::command() const {
	return m_command;
}

QString Request::queryId() const {
	return m_queryId;
}

QString Request::jwt() const {
	return m_jwt;
}

RequestPayLoad * Request::payload() const {
	return m_payload;
}

Json Request::toJson() {
	Json ret{{"command", m_command.toStdString()},
			 {"query_id", m_queryId.toStdString()}};

	if (!m_jwt.isEmpty())
		ret["jwt"] = m_jwt.toStdString();

	if (m_payload != nullptr)
		ret["payload"] = m_payload->toJson();

	std::cout << ret << std::endl;

	return ret;
}

void Request::setCommand(QString command) {
	if (m_command == command)
		return;

	m_command = command;
	emit commandChanged(m_command);
}

void Request::setQueryId(QString requestId) {
	if (m_queryId == requestId)
		return;

	m_queryId = requestId;
	emit queryIdChanged(m_queryId);
}

void Request::setJwt(QString jwt) {
	if (m_jwt == jwt)
		return;

	m_jwt = jwt;
	emit jwtChanged(m_jwt);
}

void Request::setPayload(RequestPayLoad * payload) {
	if (m_payload == payload)
		return;

	m_payload = payload;
	emit payloadChanged(m_payload);
}
