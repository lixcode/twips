#include "tcpsocket.h"

#include "request/request.h"
#include "response/response.h"

#include <iostream>

#include <QDebug>

TcpSocket::TcpSocket(QObject * parent)
	: QObject(parent) {
	//	connect(
	//	  &socket, &QWebSocket::binaryMessageReceived, this,
	//	  &WebSocket::binaryMessageReceived);
	connect(&socket, &QTcpSocket::connected, this, &TcpSocket::connected);
	connect(&socket, &QTcpSocket::disconnected, this, &TcpSocket::disconnected);
	connect(
	  &socket, QOverload<QAbstractSocket::SocketError>::of(&QTcpSocket::error),
	  this, &TcpSocket::error);
	connect(&socket, &QTcpSocket::readyRead, this, &TcpSocket::readyRead);
}

int TcpSocket::send(Request * request) {
	// Get an id and add to queue in thread safe mode
	listMutex.lock();
	int requestId = ++lastId;
	request->setQueryId(QString::number(requestId));
	list.append(RequestItem{request});
	listMutex.unlock();

	processNow();

	return requestId;
}

void TcpSocket::cancel(int request_id) {
	removeRequest(QString::number(request_id));
}

void TcpSocket::binaryMessageReceived(const QByteArray & message) {
	Json       data     = Json::from_msgpack(message);
	Response * response = new Response{this};

	std::cout << data << std::endl;
	response->loadJson(data);

	bool inQueue = false;

	for (auto & req : list) {
		inQueue = inQueue || req.request->queryId() == response->queryId();
	}

	if (inQueue) {
		emit ready(response);
		removeRequest(response->queryId());
	}
}

void TcpSocket::connected() {
	if (!list.isEmpty()) {
		processNow();
	}
}

void TcpSocket::disconnected() {
	QList<QString> ids;
	QList<QString> commands;

	if (list.isEmpty())
		return;

	for (auto & req : list) {
		ids << req.request->queryId();
		commands << req.request->command();
	}

	for (int i = 0; i < ids.length(); i++) {
		Response * response = new Response{this};

		response->setCommand(commands[i]);
		response->setQueryId(ids[i]);
		response->setCode(500);
		response->setDescription(socket.errorString());

		emit ready(response);
		removeRequest(ids[i]);
	}
}

void TcpSocket::error(QAbstractSocket::SocketError) {
	qDebug() << socket.errorString() << socket.error();
	disconnected();
}

void TcpSocket::readyRead() {
	bytesReady(socket.bytesAvailable());
}

void TcpSocket::bytesReady(qint64 bytes) {

	// handle the length of message

	if (messageLength == 0) {
		// The length is complete
		if (messageBuffer.length() + bytes >= 11) {
			int bytesForLength = 11 - messageBuffer.size();

			messageBuffer.append(socket.read(bytesForLength));

			messageLength = messageBuffer.toInt(nullptr, 10);
			messageBuffer.clear();

			bytes -= bytesForLength;
		}
		// No enough bytes for length
		else {
			messageBuffer.append(socket.read(bytes));
			bytes = 0;
		}
	}

	// handle the body of message

	if (messageLength != 0 && bytes > 0) {
		// The message body is compelete
		if (bytes + messageBuffer.length() <= messageLength) {
			int bytesForBuffer = messageLength - messageBuffer.size();

			messageBuffer.append(socket.read(bytesForBuffer));

			binaryMessageReceived(messageBuffer);
			messageBuffer.clear();

			bytes -= bytesForBuffer;
			messageLength = 0;
		}
		// No enough bytes for message body
		else {
			messageBuffer.append(socket.read(bytes));
			bytes = 0;
		}
	}

	// There is some bytes for the next message
	if (bytes > 0) {
		bytesReady(bytes);
	}
}

void TcpSocket::processNow() {
	if (socket.state() != QAbstractSocket::ConnectedState) {
		socket.connectToHost("staging.ma3ka.com", 6000);
		return;
	}

	listMutex.lock();

	for (auto & req : list) {
		if (!req.inited) {
			// The message to send
			std::vector<std::uint8_t> messagePack =
			  Json::to_msgpack(req.request->toJson());
			// The length like string (ASCII / Latin1)
			QByteArray length = QString::number(messagePack.size())
								  .rightJustified(11, '0', true)
								  .toLatin1();

			// Send the length firs
			socket.write(length);

			// Write the content, std.vector is not complete compatible with Qt,
			// but reinterpret_cast saves the situation
			socket.write(
			  reinterpret_cast<char *>(messagePack.data()), messagePack.size());

			req.inited = true;
		}
	}

	listMutex.unlock();
}

void TcpSocket::removeRequest(const QString & id) {
	listMutex.lock();

	for (auto it = list.begin(); it != list.end(); it++) {
		if (it->request->queryId() == id) {
			list.removeOne(*it);
		}
	}

	listMutex.unlock();
}
