#include "networkplugin.h"

#include "request/presencereqpl.h"
#include "request/profileupdatereqpl.h"
#include "request/request.h"
#include "request/requestcodereqpl.h"
#include "request/requestjwtreqpl.h"
#include "response/hellorespl.h"
#include "response/requestcoderespl.h"
#include "response/requestjwtrespl.h"
#include "response/response.h"
#include "tcpsocket.h"

#include <QQmlEngine>

NetworkPlugin::NetworkPlugin(QObject * parent)
	: QQmlExtensionPlugin(parent) {}

void NetworkPlugin::registerTypes(const char * uri) {
	// Request classes

	qmlRegisterType<PresenceReqPL>(uri, 1, 0, "PresenceReqPL");
	qmlRegisterType<ProfileUpdateReqPL>(uri, 1, 0, "ProfileUpdateReqPL");
	qmlRegisterType<Request>(uri, 1, 0, "Request");
	qmlRegisterType<RequestCodeReqPL>(uri, 1, 0, "RequestCodeReqPL");
	qmlRegisterType<RequestJwtReqPL>(uri, 1, 0, "RequestJwtReqPL");
	qmlRegisterType<RequestPayLoad>();

	// Response classes

	qmlRegisterType<HelloResPL>(uri, 1, 0, "HelloResPL");
	qmlRegisterType<RequestCodeResPL>(uri, 1, 0, "RequestCodeResPL");
	qmlRegisterType<RequestJwtResPL>(uri, 1, 0, "RequestJwtResPL");
	qmlRegisterType<Response>(uri, 1, 0, "Response");
	qmlRegisterType<ResponsePayLoad>();

	// Socket classes

	qmlRegisterType<TcpSocket>(uri, 1, 0, "TcpSocket");
}
