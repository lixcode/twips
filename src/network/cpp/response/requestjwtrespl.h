#ifndef REQUESTJWTRESPL_H
#define REQUESTJWTRESPL_H

#include "responsepayload.h"

/**
 * @brief The RequestJwtResPL class is payload for request_jwt response
 */
class RequestJwtResPL : public ResponsePayLoad
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString jwt READ jwt    NOTIFY jwtChanged)
	Q_PROPERTY(int userId  READ userId NOTIFY userIdChanged)
	// clang-format on

public:
	explicit RequestJwtResPL(QObject * parent = nullptr);

	QString jwt() const;
	int     userId() const;

	/**
	 * @brief loadJson copy JSON data to class fields
	 * @param json is the source of data
	 */
	void loadJson(const Json & json);

signals:
	void jwtChanged(QString jwt);
	void userIdChanged(int userId);

public slots:

private:
	QString m_jwt;
	int     m_userId;
};

#endif  // REQUESTJWTRESPL_H
