#ifndef HELLORESPL_H
#define HELLORESPL_H

#include "responsepayload.h"

/**
 * @brief The HelloResPL class is payload for hello response
 */
class HelloResPL : public ResponsePayLoad
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString message READ message NOTIFY messageChanged)
	// clang-format on

public:
	explicit HelloResPL(QObject * parent = nullptr);

	QString message() const;

	/**
	 * @brief loadJson copy JSON data to class fields
	 * @param json is the source of data
	 */
	void loadJson(const Json & json);

signals:
	void messageChanged(QString message);

public slots:

private:
	QString m_message;
};

#endif  // HELLORESPL_H
