#ifndef RESPONSE_H
#define RESPONSE_H

#include <json/json.h>

#include <QObject>

class ResponsePayLoad;

/**
 * @brief The Response class
 */
class Response : public QObject
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString command          READ command     WRITE setCommand     NOTIFY commandChanged)
	Q_PROPERTY(QString queryId          READ queryId     WRITE setQueryId     NOTIFY queryIdChanged)
	Q_PROPERTY(int code                 READ code        WRITE setCode        NOTIFY codeChanged)
	Q_PROPERTY(QString description      READ description WRITE setDescription NOTIFY descriptionChanged)
	Q_PROPERTY(ResponsePayLoad* payload READ payload     WRITE setPayload     NOTIFY payloadChanged)
	// clang-format on

public:
	explicit Response(QObject * parent = nullptr);

	QString command() const;
	QString queryId() const;
	int     code() const;
	QString description() const;

	ResponsePayLoad * payload() const;

	/**
	 * @brief loadJson copy JSON data to class fields
	 * @param json is the source of data
	 */
	void loadJson(const Json & json);

signals:
	void commandChanged(QString command);
	void queryIdChanged(QString queryId);
	void codeChanged(int code);
	void descriptionChanged(QString description);
	void payloadChanged(ResponsePayLoad * payload);

public slots:
	void setCommand(QString command);
	void setQueryId(QString queryId);
	void setCode(int code);
	void setDescription(QString description);
	void setPayload(ResponsePayLoad * payload);

private:
	QString m_command;
	QString m_queryId;
	int     m_code;
	QString m_description;

	ResponsePayLoad * m_payload = nullptr;
};

#endif  // RESPONSE_H
