#include "requestcoderespl.h"

RequestCodeResPL::RequestCodeResPL(QObject * parent)
	: ResponsePayLoad(parent) {}

QString RequestCodeResPL::token() const {
	return m_token;
}

void RequestCodeResPL::loadJson(const Json & json) {
	if (json.count("token") > 0)
		m_token = QString::fromStdString(json["token"]);
}
