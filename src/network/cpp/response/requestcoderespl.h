#ifndef REQUESTCODERESPL_H
#define REQUESTCODERESPL_H

#include "responsepayload.h"

/**
 * @brief The RequestCodeResPL class is payload for request_code response
 */
class RequestCodeResPL : public ResponsePayLoad
{
	Q_OBJECT

	// clang-format off
	Q_PROPERTY(QString token READ token NOTIFY tokenChanged)
	// clang-format on

public:
	explicit RequestCodeResPL(QObject * parent = nullptr);

	QString token() const;

	/**
	 * @brief loadJson copy JSON data to class fields
	 * @param json is the source of data
	 */
	void loadJson(const Json & json);

signals:
	void tokenChanged(QString token);

public slots:

private:
	QString m_token;
};

#endif  // REQUESTCODERESPL_H
