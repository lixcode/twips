#include "response.h"

#include "hellorespl.h"
#include "requestcoderespl.h"
#include "requestjwtrespl.h"

Response::Response(QObject * parent)
	: QObject(parent) {}

QString Response::command() const {
	return m_command;
}

QString Response::queryId() const {
	return m_queryId;
}

int Response::code() const {
	return m_code;
}

QString Response::description() const {
	return m_description;
}

ResponsePayLoad * Response::payload() const {
	return m_payload;
}

void Response::loadJson(const Json & json) {
	m_command     = QString::fromStdString(json["command"]);
	m_queryId     = QString::fromStdString(json["query_id"]);
	m_code        = json["code"];
	m_description = QString::fromStdString(json["description"]);

	if (m_command == "hello") {
		m_payload = new HelloResPL(this);
	}
	else if (m_command == "request_code") {
		m_payload = new RequestCodeResPL(this);
	}
	else if (m_command == "request_jwt") {
		m_payload = new RequestJwtResPL(this);
	}

	if (m_payload != nullptr) {
		m_payload->loadJson(json["payload"]);
	}
}

void Response::setCommand(QString command) {
	if (m_command == command)
		return;

	m_command = command;
	emit commandChanged(m_command);
}

void Response::setQueryId(QString requestId) {
	if (m_queryId == requestId)
		return;

	m_queryId = requestId;
	emit queryIdChanged(m_queryId);
}

void Response::setCode(int code) {
	if (m_code == code)
		return;

	m_code = code;
	emit codeChanged(m_code);
}

void Response::setDescription(QString description) {
	if (m_description == description)
		return;

	m_description = description;
	emit descriptionChanged(m_description);
}

void Response::setPayload(ResponsePayLoad * payload) {
	if (m_payload == payload)
		return;

	m_payload = payload;
	emit payloadChanged(m_payload);
}
