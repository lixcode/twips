#include "hellorespl.h"

HelloResPL::HelloResPL(QObject * parent)
	: ResponsePayLoad(parent) {}

QString HelloResPL::message() const {
	return m_message;
}

void HelloResPL::loadJson(const Json & json) {
	if (json.count("message") > 0)
		m_message = QString::fromStdString(json["message"]);
}
