#include "requestjwtrespl.h"

RequestJwtResPL::RequestJwtResPL(QObject * parent)
	: ResponsePayLoad(parent) {}

QString RequestJwtResPL::jwt() const {
	return m_jwt;
}

int RequestJwtResPL::userId() const {
	return m_userId;
}

void RequestJwtResPL::loadJson(const Json & json) {
	if (json.count("jwt") > 0)
		m_jwt = QString::fromStdString(json["jwt"]);
	if (json.count("user_id") > 0)
		m_userId = json["user_id"];
}
