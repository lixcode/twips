#ifndef RESPONSEPAYLOAD_H
#define RESPONSEPAYLOAD_H

#include <json/json.h>

#include <QObject>

/**
 * @brief The ResponsePayLoad class is a parent class for all response payloads
 */
class ResponsePayLoad : public QObject
{
	Q_OBJECT
public:
	explicit ResponsePayLoad(QObject * parent = nullptr);

	/**
	 * @brief loadJson copy JSON data to class fields
	 * @param json is the source of data
	 */
	virtual void loadJson(const Json & json) = 0;

signals:

public slots:
};

#endif  // RESPONSEPAYLOAD_H
