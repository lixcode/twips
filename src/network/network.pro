TARGET = twipsNetwork
TEMPLATE = lib
CONFIG += plugin

QT = core qml websockets

TWIPS_ROOT = $$PWD/../..

uri = Twips.Network
QML_TYPES = network.qmltypes
include($$TWIPS_ROOT/pri_files/qmlplugin.pri)

DISTFILES += \
    qmldir \
    network.qmltypes
 
HEADERS += \
    cpp/tcpsocket.h \
    cpp/request/presencereqpl.h \
    cpp/request/profileupdatereqpl.h \
    cpp/request/request.h \
    cpp/request/requestcodereqpl.h \
    cpp/request/requestjwtreqpl.h \
    cpp/request/requestpayload.h \
    cpp/response/hellorespl.h \
    cpp/response/requestcoderespl.h \
    cpp/response/requestjwtrespl.h \
    cpp/response/response.h \
    cpp/response/responsepayload.h \
    cpp/networkplugin.h

SOURCES += \
    cpp/tcpsocket.cpp \
    cpp/request/presencereqpl.cpp \
    cpp/request/profileupdatereqpl.cpp \
    cpp/request/request.cpp \
    cpp/request/requestcodereqpl.cpp \
    cpp/request/requestjwtreqpl.cpp \
    cpp/request/requestpayload.cpp \
    cpp/response/hellorespl.cpp \
    cpp/response/requestcoderespl.cpp \
    cpp/response/requestjwtrespl.cpp \
    cpp/response/response.cpp \
    cpp/response/responsepayload.cpp \
    cpp/networkplugin.cpp
