TEMPLATE = subdirs

SUBDIRS = main json network

main.subdir = src/main
json.subdir = src/json
network.subdir = src/network

main.depends = json network
